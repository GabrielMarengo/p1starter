package edu.uprm.ece.icom4035.polynomial;

import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.ece.icom4035.list.List;
import edu.uprm.ece.icom4035.list.SinglyLinkedList.Node;

public class PolynomialImp implements Polynomial {
	
	private List<Term> list;
	
	public PolynomialImp(String str) {
		list = TermMaker(str);
	}
	
	public PolynomialImp(List<Term> list) {
		this.list = list;
	}
	
	
	/**
	 * 
	 * @param str is the string to be converted into a polynomial
	 * @return a List<Term> with the polynomial divided into terms
	 */
	public List<Term> TermMaker(String str) {
		List<Term> termList = new TermListFactory().newListFactory().newInstance();
		
		String arr[] = str.split("\\+");
		for(int i = 0; i < arr.length; i++) {
			String coe = "";
			String exp = "";
			if(arr[i].length() == 1 && arr[i].charAt(arr[i].length()-1) == 'x') {
				coe = "1";
				exp = "1";
			}else if(arr[i].charAt(arr[i].length()-1) == 'x') {
				coe = arr[i].substring(0, arr[i].length()-1);
				exp = "1";
				
			}else {
				String[] term = arr[i].split("x\\^");
				if(term.length == 1) {
					coe = term[0];
					exp = "0";
				}else {
					coe = term[0];
					exp = term[1];
				}if(term[0].equals(""))
					coe = "1";
			}
			
			if(!coe.equals("0")) {
				termList.add(new TermImp(Double.parseDouble(coe), Integer.parseInt(exp)));
			}
			
		}
		
		return termList;
		
	}
	
	
	
	
	
	public List<Term> getList(){
		return list;
	}
	
	@Override
	public Iterator<Term> iterator() {
		return this.list.iterator();
	}

	@Override
	public Polynomial add(Polynomial P2) {
		List<Term> newList = new TermListFactory().newListFactory().newInstance();
		

		
		for(int i = 0; i < this.getList().size(); i++)
			newList.add(this.getList().get(i));
		for(int i = 0; i < ((PolynomialImp) P2).getList().size(); i++)
			newList.add(((PolynomialImp) P2).getList().get(i));
		
		
		return new PolynomialImp(this.addAndSort(newList));
	}
	

	/**
	 * 
	 * @param list is the list to be modified
	 * @return a new list which has had all the terms with the same exponent added, removed the ones with coefficient 0
	 * and organized it using the exponents
	 */
	public List<Term> addAndSort(List<Term> list){
		
		//add elements with the same exponent
		for(int i = 0; i < list.size()-1;i++)
			for(int j = i+1; j < list.size(); j++) {
				if(list.get(i).getExponent() == list.get(j).getExponent()) {
					list.set(i, new TermImp(list.get(i).getCoefficient() + list.get(j).getCoefficient(), list.get(i).getExponent()));
					list.remove(j);
				}	
			}
		
		//removes all elements with coefficient 0
		boolean noZero = false;
		while(!noZero) {
			noZero = true;
			for(int i = 0; i < list.size(); i++) {
				if(list.get(i).getCoefficient() == 0) {
					list.remove(list.get(i));
					noZero = false;
				}
			}
		}
		
		
		//sorts
		int n = list.size(); 
        for (int i=1; i<n; ++i) 
        { 
            Term key = list.get(i); 
            int j = i-1; 
            while (j>=0 && list.get(j).getExponent() < key.getExponent()) 
            { 
                list.set(j+1, list.get(j)); 
                j = j-1; 
            } 
            list.set(j+1, key); 
        } 
		
    	
		return list;
		
	}



	@Override
	public Polynomial subtract(Polynomial P2) {
		return this.add(P2.multiply(-1));
	}

	@Override
	public Polynomial multiply(Polynomial P2) {
		List<Term> newList = new TermListFactory().newListFactory().newInstance();

		PolynomialImp newP = (PolynomialImp) P2;
		for(int i = 0; i < this.getList().size(); i++) {
			for(int j = 0; j < newP.getList().size();j++) {
				newList.add(new TermImp(this.getList().get(i).getCoefficient()* newP.getList().get(j).getCoefficient(), this.getList().get(i).getExponent() + newP.getList().get(j).getExponent()));
			}
		}
		
		
		return new PolynomialImp(this.addAndSort(newList));
	}

	@Override
	public Polynomial multiply(double c) {

		List<Term> newList = new TermListFactory().newListFactory().newInstance();

		
		for(int i = 0; i < this.getList().size(); i++)
			newList.add(new TermImp(c * this.getList().get(i).getCoefficient(), this.getList().get(i).getExponent()));
			
		
		return new PolynomialImp(this.addAndSort(newList));
	}

	@Override
	public Polynomial derivative() {
		List<Term> newList = new TermListFactory().newListFactory().newInstance();
		for(Term term: this) {
			newList.add(new TermImp(term.getCoefficient()*(term.getExponent()), term.getExponent()-1));
		}
		
		
		return new PolynomialImp(this.addAndSort(newList));
	}

	@Override
	public Polynomial indefiniteIntegral() {
		List<Term> newList = new TermListFactory().newListFactory().newInstance();

		
		for(int i = 0; i < this.getList().size(); i++){
			newList.add(new TermImp(this.getList().get(i).getCoefficient()/(this.getList().get(i).getExponent()+1), this.getList().get(i).getExponent()+1));
		}
		newList.add(new TermImp(1, 0));
		return new PolynomialImp(newList);
	}

	@Override
	public double definiteIntegral(double a, double b) {
		return Math.abs(this.indefiniteIntegral().evaluate(a) - this.indefiniteIntegral().evaluate(b));
	}

	@Override
	public int degree() {
		return this.list.first().getExponent();
	}

	@Override
	public double evaluate(double x) {
		double sum = 0;
		
		for(int i = 0; i < this.getList().size(); i++)
			sum += this.getList().get(i).evaluate(x);
		return sum;
	}

	@Override
	public boolean equals(Polynomial P) {
		return this.toString().equals(P.toString());
	}
	
	@Override
	public String toString() {
		String output = "";
		for(int i = 0; i < this.list.size(); i++) {
			if(i == 0) {
			}else {
				output += "+";
			}

			
			String str = String.format("%.2f", this.getList().get(i).getCoefficient());
			if(this.getList().get(i).getExponent() ==1){
				output += str + "x";			 
			}else if(this.getList().get(i).getExponent() != 0){
				output += str + "x^" + String.valueOf(this.getList().get(i).getExponent());			 
			}else {
				output += str;
			}
			
		}
		return output;
		
	}

}
