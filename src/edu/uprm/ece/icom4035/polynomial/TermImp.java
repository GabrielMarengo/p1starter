package edu.uprm.ece.icom4035.polynomial;

public class TermImp implements Term{
	public double coefficient;
	public int exponent;
	
	public TermImp(double coefficient, int exponent) {
		this.coefficient = coefficient;
		this.exponent = exponent;
	}
	
	@Override
	public double getCoefficient() {
		return coefficient;
	}
	@Override
	public int getExponent() {
		return exponent;
	}
	public void setCoefficient(double coefficient) {
		this.coefficient = coefficient;
	}
	public void setExponent(int exponent) {
		this.exponent = exponent;
	}
	@Override
	public double evaluate(double x) {
		return this.getCoefficient()*(Math.pow(x,this.getExponent()));
	}
	
	

}
