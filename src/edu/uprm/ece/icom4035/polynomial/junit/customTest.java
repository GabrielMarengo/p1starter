package edu.uprm.ece.icom4035.polynomial.junit;

import edu.uprm.ece.icom4035.list.ArrayList;
import edu.uprm.ece.icom4035.list.List;
import edu.uprm.ece.icom4035.polynomial.PolynomialImp;
import edu.uprm.ece.icom4035.polynomial.Term;

public class customTest {
	
	public static void main (String args[]) {
		PolynomialImp poly = new PolynomialImp("8x^2+1");
		System.out.println(poly.toString());
		ArrayList<Term> termList = (ArrayList<Term>) poly.addAndSort(poly.getList());
		poly = new PolynomialImp(termList);
		poly = (PolynomialImp) poly.derivative();
		
		System.out.println(poly.toString());
		
	}

}
