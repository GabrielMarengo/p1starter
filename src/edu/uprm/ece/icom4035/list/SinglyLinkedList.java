package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.ece.icom4035.list.SinglyLinkedList.Node;
import edu.uprm.ece.icom4035.list.SinglyLinkedList.SLLIterator;

public class SinglyLinkedList<E> implements List<E> {

	public static class Node<E>{
		private E element;
		private Node<E> next;

		public Node() {
			this.element = null;
			this.next = null;
		}

		public Node(E element, Node<E> N) {
			this.element = element;
			this.next = N;
		}

		public E getElement() {
			return element;
		}

		public void setElement(E element) {
			this.element = element;
		}

		public Node<E> getNext() {
			return next;
		}

		public void setNext(Node<E> next) {
			this.next = next;
		}


	}
	/**
	 * 
	 * Iterator for the SLL
	 *
	 * @param <E>
	 */
	public class SLLIterator<E> implements Iterator<E> {
		private Node<E> currentNode;

		private SLLIterator() {
			this.currentNode = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return currentNode.getNext() != null;

		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result= null;
				result = this.currentNode.getElement();
				this.currentNode = this.currentNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}

	private Node<E> header;
	private int size;

	
	/**
	 * Constructor
	 */
	public SinglyLinkedList() {
		this.header = new Node<E>();
		this.size = 0;
	}

	@Override
	public Iterator iterator() {
		return new SLLIterator<E>();
	}

	@Override
	public void add(E e) {
		if (this.isEmpty()) {
			Node<E> temp= new Node(e, null);
			this.header.setNext(temp);

		}
		else {
			Node<E> newNode = new Node<E>((E) e,null );
			Node<E> temp = this.findNode(this.size() - 1);
			temp.setNext(newNode);
		}
		this.size++;
	}

	@Override
	public void add(int index, E e) {
		if (index == this.size()) {
			this.add(e);
			return;
		}

		this.checkIndex(index);

		if (index == 0) {
			Node<E> newNode = new Node(e, null);
			newNode.setNext(this.header.getNext());
			this.header.getNext().setNext(newNode);
		}
		else {
			Node<E> newNode = new Node(e, null);
			Node<E> temp = this.findNode(index);
			newNode.setNext(temp.getNext());
			temp.setNext(newNode);
		}
		this.size++;
	}

	@Override
	public boolean remove(E e) {
		
		if(!this.contains(e)) return false;
		else
			return this.remove(this.firstIndex(e));
	}

	@Override
	public boolean remove(int index) {
		
		if(index < 0 || index >= this.size) throw new IndexOutOfBoundsException();
		Node<E> rem = this.findNode(index);
		if(index==0) {
			this.header.setNext(this.findNode(index+1));
			
			rem.setNext(null);
			rem.setElement(null);
			size--;
			return true;
		}
		if(this.contains(rem.getElement())) {
			this.findNode(index - 1).setNext(this.findNode(index+ 1));
			
			rem.setNext(null);
			rem.setElement(null);
			size--;
			return true;
		}
		
		return false;
	}

	@Override
	public int removeAll(E e) {
		if(this.remove(e))
			return 1 + this.removeAll(e);
		else
			return 0;
	}

	@Override
	public E get(int index) {
		this.checkIndex(index);
		Node<E> target = this.findNode(index);
		return target.getElement();
	}

	@Override
	public E set(int index, E e) {
		this.checkIndex(index);
		Node<E> target = this.findNode(index);
		Node<E> oldNode = target;
		target.setElement(e);
		return oldNode.getElement();
	}

	@Override
	public E first() {
		if(isEmpty()){
			return null;
		}
		else{
			return header.getNext().getElement();
		}
	}

	@Override
	public E last() {
		return  this.findNode(size-1).getElement();
	}

	@Override
	public int firstIndex(E e) {
		Node<E> temp = header.getNext();
		for(int i = 0; i < size; i++) {
			if(temp.getElement() == e)
				return i;
			else{
				temp = temp.getNext();
			}
		}

		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int idx = this.firstIndex(e);
		if(idx == -1)
			return -1;
		Node<E> temp = this.findNode(idx);
		for(int i = idx; i < size; i++) {
			if(temp.getElement() == e)
			idx = i;
			else{
				temp = temp.getNext();
			}
		}
		return idx;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public boolean contains(E e) {
		if(this.firstIndex(e) == -1)
			return false;
		return true;
	}

	@Override
	public void clear() {
		Node<E> temp = header.getNext();
		for(int i = 0; i < size; i++) {
			temp.setElement(null);
			temp = temp.getNext();
		}
		size=0;
	}

	private Node<E> findNode(int index) {
		//METODO PARA ENCONTRAR EL NODO
		Node<E> temp = this.header.getNext();
		int i = 0;

		while (i < index) {
			temp = temp.getNext();
			i++;
		}
		return temp;
	}

	private void checkIndex(int index) {
		//METODO PARA VER SI EXISTE EL NODO
		if ((index < 0) || (index >= this.size())){
			throw new IndexOutOfBoundsException();
		}
	}

}