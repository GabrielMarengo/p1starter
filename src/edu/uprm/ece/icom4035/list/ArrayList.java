 package edu.uprm.ece.icom4035.list;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayList<T> implements List<T>{
	
	private static final int INITCAP = 5;
	private T elements[];
	private int size;
	
	
	private class ListIterator<T> implements Iterator<T>{
		private int currentPosition;
		
		public ListIterator(){
			this.currentPosition = 0;
		}
		@Override
		public boolean hasNext() {
			return this.currentPosition < size();
		}

		@Override
		public T next() {
			if (this.hasNext()) {
				T result = null;
				result = (T) elements[this.currentPosition++];
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}

	
	public ArrayList() {
		this.elements= (T[]) new Object[INITCAP];
		this.size = 0;
	}
	public ArrayList(int initCapacity) {
		if (initCapacity < 1) {
			throw new IllegalArgumentException
			("Initial capacity must be at least 1");
		}
		this.elements= (T[]) new Object[initCapacity];
		this.size = 0;
	}	
	
	
	@Override
	public Iterator iterator() {
		return new ListIterator<T>();
	}

	@Override
	public void add(T e) {
		if (this.size() == this.elements.length) {
			this.changeCapacity(2 * this.size());
		}
		this.elements[this.size++] = e;
		
	}

	

	@Override
	public void add(int index, T e) {
		if ((index < 0) || (index > this.size())) {
			throw new IndexOutOfBoundsException();
		}
		if (this.size() == this.elements.length) {
			this.changeCapacity(2 * this.size());
		}
		if (index == this.size()) {
			this.add(e);
			return;
		}
		for (int i= this.size(); i > index ; --i) {
			this.elements[i] = this.elements[i-1];
		}
		this.elements[index] = e;
		this.size++;
		
	}

	@Override
	public boolean remove(T e) {
		if(this.firstIndex(e) != -1)
			return this.remove(this.firstIndex(e));
		return false;
		
	}

	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException {
		if ((index < 0 ) || (index >= this.size())) {
			throw new IndexOutOfBoundsException();
		}
		for (int i=index; i < this.size() - 1;++i) {
			this.elements[i] = this.elements[i+1];
		}
		this.elements[this.size()-1] = null;
		this.size--;
		return true;
	}

	@Override
	public int removeAll(T e) {
		int amount = 0;
		while(this.firstIndex(e) != -1) {
			this.remove(e);
			amount++;
		}
		
		return amount;
	}

	@Override
	public T get(int index) {
		if ((index < 0 ) || (index >= this.size())) {
			throw new IndexOutOfBoundsException();
		}
		return this.elements[index];
	}

	@Override
	public T set(int index, T e)  throws IndexOutOfBoundsException {
		if ((index < 0 ) || (index >= this.size())) {
			throw new IndexOutOfBoundsException();
		}
		T result = this.elements[index];
		this.elements[index] = e;
		return result;
	}

	@Override
	public T first() {
		if(this.isEmpty())
			return null;
		return this.get(0);
	}

	@Override
	public T last() {
		if(this.isEmpty())
			return null;
		return this.get(size-1);
	}

	@Override
	public int firstIndex(T e) {
		for(int i = 0; i < size; i++)
			if(this.elements[i] == e)
				return i;
		
		return -1;
	}

	@Override
	public int lastIndex(T e) {
		for(int i = size-1; i >= 0; i--)
			if(this.elements[i] == e)
				return i;
		
		return -1;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public boolean contains(T e) {
		if(this.firstIndex(e) != -1)
			return true;
		return false;
	}

	@Override
	public void clear() {
		for(T ele: this) {
			ele = null;
		}
		size = 0;
		
	}
	
	private void changeCapacity(int newCapacity) {
		T [] temp = (T[]) new Object[newCapacity];
		for (int i=0; i < this.size(); ++i) {
			temp[i] = this.elements[i];
		}
		this.elements = temp;
		
	}

}
